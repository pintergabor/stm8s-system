#ifndef __inline_h
#define __inline_h

/**
 * @file
 * Simple inline functions
 ******************************************************************************/


/**
 * No operation
 * Takes one cycle.
 ******************************************************************************/
#define nop()   { _asm("nop"); }

/**
 * Flush cache
 * Takes two cycles.
 ******************************************************************************/
#define flush()     { _asm("jra $N\n $L:"); }

/**
 * Align
 ******************************************************************************/
#define align(n)    { _asm("align "#n", 0x9D"); }

/**
 * Align and flush cache
 * Takes two cycles.
 ******************************************************************************/
#define aflush(n)   { _asm("jra $N\n align "#n", 0x9D\n $L:"); }

/**
 * Load A
 ******************************************************************************/
#define lda(v)      ((byte)_asm("", (byte)(v)))

/**
 * Load X
 ******************************************************************************/
#define ldwx(v)     ((word)_asm("", (word)(v)))

/**
 * Exchange X and Y
 ******************************************************************************/
#define exgwxy()    { _asm("exgw X, Y"); }

/**
 * Get CC
 ******************************************************************************/
#define getcc()     ((byte)_asm("push CC\n pop A"))

/**
 * Set CC
 ******************************************************************************/
#define setcc(v)    (_asm("push A\n pop CC", (byte)(v)))

/**
 * Disable interrupts (= Set interrupt level 3)
 ******************************************************************************/
#define di()        { _asm("sim"); }

/**
 * Enable interrupts (= Set interrupt level 0)
 ******************************************************************************/
#define ei()        { _asm("rim"); }

/**
 * Set interrupt level
 ******************************************************************************/
#define seti3()     { _asm("sim"); }
#define seti2()     { _asm("push 0x00\n pop CC"); }
#define seti1()     { _asm("push 0x08\n pop CC"); }
#define seti0()     { _asm("rim"); }

/**
 * Disable interrupts, and return current interrupt state
 ******************************************************************************/
#define diex()      ((byte)_asm("push CC\n pop A\n sim"))

/**
 * Enable interrupts by restoring previous interrupt state
 ******************************************************************************/
#define eiex(s)     ((void)_asm("push A\n pop CC", (byte)(s)))

/**
 * Wait for event
 ******************************************************************************/
#define wfe()       { _asm("wfe"); }

/**
 * Wait for interrupt
 ******************************************************************************/
#define wfi()       { _asm("wfi"); }

/**
 * Wait for external interrupt
 ******************************************************************************/
#define halt()      { _asm("halt"); }

/**
 * Illegal opcode
 ******************************************************************************/
#define illop()     { _asm("dc.b 0x75"); }

/**
 * Swap upper and lower nibbles of a byte
 * On a more intelligent compiler it could be written as:
 *   (byte)(((byte)v << 4)|((byte)v >> 4))
 ******************************************************************************/
#define swap(v)     ((byte)_asm("swap A", v))

/**
 * Swap upper and lower bytes of a word
 * On a more intelligent compiler it could be written as:
 *   (word)(((word)v << 8)|((word)v >> 8))
 ******************************************************************************/
#define swapw(v)    ((word)_asm("swapw X", v))

/**
 * Rotate left byte
 * v must be a byte variable.
 * On a more intelligent compiler it could be written as:
 *   v= (byte)((v << 1)|(v >> 7))
 ******************************************************************************/
#define rlb(v)      { _asm("rlc _"#v); _asm("bccm _"#v",#0"); }

/**
 * Rotate right byte
 * v must be a byte variable.
 * On a more intelligent compiler it could be written as:
 *   v= (byte)((v << 7)|(v >> 1))
 ******************************************************************************/
#define rrb(v)      { _asm("rrc _"#v); _asm("bccm _"#v",#7"); }

/**
 * Rotate left word
 * v must be a word variable.
 * On a more intelligent compiler it could be written as:
 *   v= (word)((v << 1)|(v >> 15))
 ******************************************************************************/
#define rlw(v)      { _asm("rlc _"#v); _asm("rlc _"#v"+1"); _asm("bccm _"#v",#0"); }

/**
 * Rotate right word
 * v must be a word variable.
 * On a more intelligent compiler it could be written as:
 *   v= (word)((v << 15)|(v >> 1))
 ******************************************************************************/
#define rrw(v)      { _asm("rrc _"#v"+1"); _asm("rrc _"#v); _asm("bccm _"#v"+1,#7"); }


/**
 * The carry function, defined by the compiler,
 * is used to test or get the carry bit from
 * the condition register. If the carry function is used in a
 * test, the compiler produces a jnrc or jrc instruction. If
 * the carry function is used in any other expression, the
 * compiler produces a code sequence setting the a register
 * to 0 or 1 depending on the carry bit value.
 ******************************************************************************/
@inline char carry(void);


#endif

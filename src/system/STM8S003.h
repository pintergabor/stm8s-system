#ifndef __STM8S003_H
#define __STM8S003_H

/**
 * @file
 * Header for STM8S003 value line.
 * Created from various example files. Please VERIFY before use.
 ******************************************************************************/


// Types
//-------------------------------------------------------------------------
typedef volatile unsigned char  hwbyte_t;
typedef volatile unsigned int   hwword_t;


// PORTS section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t ODR_D0             :1;   // 00.0   // Output Data Reg, bit 0
        hwbyte_t ODR_D1             :1;   // 00.1   // Output Data Reg, bit 1
        hwbyte_t ODR_D2             :1;   // 00.2   // Output Data Reg, bit 2
        hwbyte_t ODR_D3             :1;   // 00.3   // Output Data Reg, bit 3
        hwbyte_t ODR_D4             :1;   // 00.4   // Output Data Reg, bit 4
        hwbyte_t ODR_D5             :1;   // 00.5   // Output Data Reg, bit 5
        hwbyte_t ODR_D6             :1;   // 00.6   // Output Data Reg, bit 6
        hwbyte_t ODR_D7             :1;   // 00.7   // Output Data Reg, bit 7
        hwbyte_t IDR_D0             :1;   // 01.0   // Input Data Reg, bit 0
        hwbyte_t IDR_D1             :1;   // 01.1   // Input Data Reg, bit 1
        hwbyte_t IDR_D2             :1;   // 01.2   // Input Data Reg, bit 2
        hwbyte_t IDR_D3             :1;   // 01.3   // Input Data Reg, bit 3
        hwbyte_t IDR_D4             :1;   // 01.4   // Input Data Reg, bit 4
        hwbyte_t IDR_D5             :1;   // 01.5   // Input Data Reg, bit 5
        hwbyte_t IDR_D6             :1;   // 01.6   // Input Data Reg, bit 6
        hwbyte_t IDR_D7             :1;   // 01.7   // Input Data Reg, bit 7
        hwbyte_t DDR_D0             :1;   // 02.0   // Data Direction Reg, bit 0
        hwbyte_t DDR_D1             :1;   // 02.1   // Data Direction Reg, bit 1
        hwbyte_t DDR_D2             :1;   // 02.2   // Data Direction Reg, bit 2
        hwbyte_t DDR_D3             :1;   // 02.3   // Data Direction Reg, bit 3
        hwbyte_t DDR_D4             :1;   // 02.4   // Data Direction Reg, bit 4
        hwbyte_t DDR_D5             :1;   // 02.5   // Data Direction Reg, bit 5
        hwbyte_t DDR_D6             :1;   // 02.6   // Data Direction Reg, bit 6
        hwbyte_t DDR_D7             :1;   // 02.7   // Data Direction Reg, bit 7
        hwbyte_t CR1_D0             :1;   // 03.0   // Configuration Reg 1, bit 0
        hwbyte_t CR1_D1             :1;   // 03.1   // Configuration Reg 1, bit 1
        hwbyte_t CR1_D2             :1;   // 03.2   // Configuration Reg 1, bit 2
        hwbyte_t CR1_D3             :1;   // 03.3   // Configuration Reg 1, bit 3
        hwbyte_t CR1_D4             :1;   // 03.4   // Configuration Reg 1, bit 4
        hwbyte_t CR1_D5             :1;   // 03.5   // Configuration Reg 1, bit 5
        hwbyte_t CR1_D6             :1;   // 03.6   // Configuration Reg 1, bit 6
        hwbyte_t CR1_D7             :1;   // 03.7   // Configuration Reg 1, bit 7
        hwbyte_t CR2_D0             :1;   // 04.0   // Configuration Reg 2, bit 0
        hwbyte_t CR2_D1             :1;   // 04.1   // Configuration Reg 2, bit 1
        hwbyte_t CR2_D2             :1;   // 04.2   // Configuration Reg 2, bit 2
        hwbyte_t CR2_D3             :1;   // 04.3   // Configuration Reg 2, bit 3
        hwbyte_t CR2_D4             :1;   // 04.4   // Configuration Reg 2, bit 4
        hwbyte_t CR2_D5             :1;   // 04.5   // Configuration Reg 2, bit 5
        hwbyte_t CR2_D6             :1;   // 04.6   // Configuration Reg 2, bit 6
        hwbyte_t CR2_D7             :1;   // 04.7   // Configuration Reg 2, bit 7
    };
    struct {
        hwbyte_t ODR;                     // 00     // Output Data Reg
        hwbyte_t IDR;                     // 01     // Input Data Reg
        hwbyte_t DDR;                     // 02     // Data Direction Reg
        hwbyte_t CR1;                     // 03     // Configuration Reg 1
        hwbyte_t CR2;                     // 04     // Configuration Reg 2
    };
} GPIO_t;
GPIO_t PA                           @0x5000;        // Port A
GPIO_t PB                           @0x5005;        // Port B
GPIO_t PC                           @0x500A;        // Port C
GPIO_t PD                           @0x500F;        // Port D
GPIO_t PE                           @0x5014;        // Port E
GPIO_t PF                           @0x5019;        // Port F


// FLASH section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_FIX            :1;   // 00.0   // FIX programming time
        hwbyte_t CR1_IE             :1;   // 00.1   // flash Interrupt Enable
        hwbyte_t CR1_AHALT          :1;   // 00.2   // standby in Active HALT mode
        hwbyte_t CR1_HALT           :1;   // 00.3   // standby in HALT mode
        hwbyte_t                    :1;   // 00.4
        hwbyte_t                    :1;   // 00.5
        hwbyte_t                    :1;   // 00.6
        hwbyte_t                    :1;   // 00.7
        hwbyte_t CR2_PRG            :1;   // 01.0   // PRoGramming block
        hwbyte_t                    :1;   // 01.1
        hwbyte_t                    :1;   // 01.2
        hwbyte_t                    :1;   // 01.3
        hwbyte_t CR2_FPRG           :1;   // 01.4   // Fast PRoGramming mode
        hwbyte_t CR2_ERASE          :1;   // 01.5   // ERASE block
        hwbyte_t CR2_WPRG           :1;   // 01.6   // Word PRoGramming
        hwbyte_t CR2_OPT            :1;   // 01.7   // select OPTion byte
        hwbyte_t NCR2_NPRG          :1;   // 02.0   // complemeNtary PRoGramming block
        hwbyte_t                    :1;   // 02.1
        hwbyte_t                    :1;   // 02.2
        hwbyte_t                    :1;   // 02.3
        hwbyte_t NCR2_NFPRG         :1;   // 02.4   // complemeNtary Fast PRoGramming mode
        hwbyte_t NCR2_NERASE        :1;   // 02.5   // complemeNtary ERASE block
        hwbyte_t NCR2_NWPRG         :1;   // 02.6   // complemeNtary Word PRoGramming
        hwbyte_t NCR2_NOPT          :1;   // 02.7   // complemeNtary select OPTion byte
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t IAPSR_WRPGDIS      :1;   // 05.0   // WRite attempted to Protected paGe
        hwbyte_t IAPSR_PUL          :1;   // 05.1   // Flash Program memory unlocked flag
        hwbyte_t IAPSR_EOP          :1;   // 05.2   // End of operation flag
        hwbyte_t IAPSR_DUL          :1;   // 05.3   // Data EEPROM unlocked flag
        hwbyte_t                    :1;   // 05.4
        hwbyte_t                    :1;   // 05.5
        hwbyte_t IAPSR_HVOFF        :1;   // 05.6   // End of high voltage flag
        hwbyte_t                    :1;   // 05.7
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
        hwbyte_t                    :8;   // 08
        hwbyte_t                    :8;   // 09
        hwbyte_t                    :8;   // 0A
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Reg 1
        hwbyte_t CR2;                     // 01     // Control Reg 2
        hwbyte_t NCR2;                    // 02     // complemeNtary Control Reg 2
        hwbyte_t FPR;                     // 03     // Flash Protection Reg
        hwbyte_t NFPR;                    // 04     // Flash complemeNtary Protection Reg
        hwbyte_t IAPSR;                   // 05     // In-Application Program Status Reg
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
        hwbyte_t PUKR;                    // 08     // Program memory Unprotection Key Reg
        hwbyte_t                    :8;   // 09
        hwbyte_t DUKR;                    // 0A     // Data EEPROM Unprotection Key Reg
    };
} FLASH_t;
FLASH_t FLASH                       @0x505A;        // Flash


// External Interrupt Controller section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_PAIS           :2;   // 00.0-1 // PORTA external Interrupt Sensitivity
        hwbyte_t CR1_PBIS           :2;   // 00.2-3 // PORTB external Interrupt Sensitivity
        hwbyte_t CR1_PCIS           :2;   // 00.4-5 // PORTC external Interrupt Sensitivity
        hwbyte_t CR1_PDIS           :2;   // 00.6-7 // PORTD external Interrupt Sensitivity
        hwbyte_t CR2_PEIS           :2;   // 00.0-1 // PORTE external Interrupt Sensitivity
        hwbyte_t CR2_TLIS           :1;   // 00.2   // Top Level Interrupt Sensitivity
        hwbyte_t                    :5;   // 00.3-7
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Reg 1
        hwbyte_t CR2;                     // 01     // Control Reg 2
    };
} EXTI_t;
EXTI_t EXTI                         @0x50A0;


// Reset Status section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t SR_WWDGF           :1;   // 00.0   // Windowed WatchDoG Reset Flag
        hwbyte_t SR_IWDGF           :1;   // 00.1   // Independent WatchDoG Reset Flag
        hwbyte_t SR_ILLOPF          :1;   // 00.2   // ILLegal OPcode Reset Flag
        hwbyte_t SR_SWIMF           :1;   // 00.3   // SWIM reset Flag
        hwbyte_t SR_EMCF            :1;   // 00.4   // EMC Reset Flag
        hwbyte_t                    :3;   // 00.5-7
    };
    struct {
        hwbyte_t SR;                      // 00     // Status Reg
    };
} RST_t;
RST_t RST                           @0x50B3;


// CLOCK section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t ICKR_HSIEN         :1;   // 00.0   // High Speed Internal oscillator ENable
        hwbyte_t ICKR_HSIRDY        :1;   // 00.1   // High Speed internal oscillator ReaDY
        hwbyte_t ICKR_FHWU          :1;   // 00.2   // Fast Wake-Up from active halt/Halt mode
        hwbyte_t ICKR_LSIEN         :1;   // 00.3   // Low Speed Internal oscillator ENable
        hwbyte_t ICKR_LSIRDY        :1;   // 00.4   // Low Speed Internal oscillator ReaDY
        hwbyte_t ICKR_SWUAH         :1;   // 00.5   // Slow Wake-Up from Active Halt/halt modes
        hwbyte_t                    :2;   // 00.6-7
        hwbyte_t ECKR_HSEEN         :1;   // 01.0   // High Speed External crystal oscillator ENable
        hwbyte_t ECKR_HSERDY        :1;   // 01.1   // High Speed External crystal oscillator ReaDY
        hwbyte_t                    :6;   // 01.2-7
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t SWCR_SWBSY         :1;   // 05.0   // SWitch BuSY flag
        hwbyte_t SWCR_SWEN          :1;   // 05.1   // SWitch start/stop ENable
        hwbyte_t SWCR_SWIEN         :1;   // 05.2   // clock SWitch Interrupt ENable
        hwbyte_t SWCR_SWIF          :1;   // 05.3   // clock SWitch Interrupt Flag
        hwbyte_t                    :4;   // 05.4-7
        hwbyte_t CKDIVR_CPUDIV      :3;   // 06.0-2 // SWitch BuSY flag
        hwbyte_t CKDIVR_HSIDIV      :2;   // 06.3-4 // clock SWitch Interrupt Flag
        hwbyte_t                    :3;   // 06.5-7
        hwbyte_t PCKENR1_I2C        :1;   // 07.0   // I2C clock enable
        hwbyte_t PCKENR1_SPI        :1;   // 07.1   // SPI clock enable
        hwbyte_t                    :1;   // 07.2
        hwbyte_t PCKENR1_UART       :1;   // 07.3   // UART clock enable
        hwbyte_t PCKENR1_TIM4       :1;   // 07.4   // TIMer 4 clock enable
        hwbyte_t PCKENR1_TIM2       :1;   // 07.5   // TIMer 2 clock enable
        hwbyte_t                    :1;   // 07.6
        hwbyte_t PCKENR1_TIM1       :1;   // 07.7   // TIMer 1 clock enable
        hwbyte_t CSSR_CSSEN         :1;   // 08.0   // Clock Security System ENable
        hwbyte_t CSSR_AUX           :1;   // 08.1   // AUXiliary oscillator connected to master clock
        hwbyte_t CSSR_CSSDIE        :1;   // 08.2   // Clock Security System Detection Interrupt Enable
        hwbyte_t CSSR_CSSD          :1;   // 08.3   // Clock Security System Detection
        hwbyte_t                    :4;   // 08.4-7
        hwbyte_t CCOR_CCOEN         :1;   // 09.0   // Configurable Clock Output ENable
        hwbyte_t CCOR_CCOSEL        :4;   // 09.1-4 // Configurable Clock Output SELection
        hwbyte_t CCOR_CCORDY        :1;   // 09.5   // Configurable Clock Output ReaDY
        hwbyte_t CCOR_CCOBSY        :1;   // 09.6   // Configurable Clock Output BuSY
        hwbyte_t                    :1;   // 09.7
        hwbyte_t                    :2;   // 0A.0-1
        hwbyte_t PCKENR2_AWU        :1;   // 0A.2   // AWU clock enable
        hwbyte_t PCKENR2_ADC        :1;   // 0A.3   // ADC clock enable
        hwbyte_t                    :4;   // 0A.4-7
        hwbyte_t                    :8;   // 0B
        hwbyte_t HSITRIMR_HSITRIM   :4;   // 0C.0-3 // High Speed Internal oscillator TRImmer
        hwbyte_t                    :4;   // 0C.4-7
        hwbyte_t SWIMCCR_SWIMDIV    :1;   // 0D.0   // SWIM clock DIViding factor
        hwbyte_t                    :7;   // 0D.1-7
    };
    struct {
        hwbyte_t ICKR;                    // 00     // Internal ClocK Control Reg
        hwbyte_t ECKR;                    // 01     // External ClocK Control Reg
        hwbyte_t                    :8;   // 02
        hwbyte_t CMSR;                    // 03     // Clock Master Status Reg
        hwbyte_t SWR;                     // 04     // master SWitch Reg
        hwbyte_t SWCR;                    // 05     // SWitch Control Reg
        hwbyte_t CKDIVR;                  // 06     // ClocK DIVider Reg
        hwbyte_t PCKENR1;                 // 07     // Peripheral ClocK ENable Reg 1
        hwbyte_t CSSR;                    // 08     // Clock Security System Reg
        hwbyte_t CCOR;                    // 09     // Configurable Clock Output Reg
        hwbyte_t PCKENR2;                 // 0A     // Peripheral ClocK ENable Reg 2
        hwbyte_t                    :8;   // 0B
        hwbyte_t HSITRIMR;                // 0C     // HSI calibration TRIMming Reg
        hwbyte_t SWIMCCR;                 // 0D     // SWIM Clock Control Reg
    };
} CLK_t;
CLK_t CLK                           @0x50C0;


// Windowed Watchdog section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t                    :6;   // 00.0-5 // Control Reg
        hwbyte_t CR_T6              :1;   // 00.6   // counter T6 bit
        hwbyte_t CR_WDGA            :1;   // 00.7   // WatchDoG Activation
        hwbyte_t WR_W               :7;   // 01.0-6 // Window
        hwbyte_t                    :1;   // 01.7
    };
    struct {
        hwbyte_t CR_T               :6;   // 00.0-5 // counter
        hwbyte_t                    :1;   // 00.7
        hwbyte_t                    :8;   // 01
    };
    struct {
        hwbyte_t CR;                      // 00     // Control Reg
        hwbyte_t WR;                      // 01     // Window Reg
    };
} WWDG_t;
WWDG_t WWDG                         @0x50D1;


// Independent Watchdog section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t KR;                      // 00     // Key Reg
        hwbyte_t PR;                      // 01     // Prescaler Reg
        hwbyte_t RLR;                     // 02     // ReLoad Reg
    };
} IWDG_t;
IWDG_t IWDG                         @0x50E0;


// Auto Wakeup section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CSR_MSR            :1;   // 00.0   // MeaSuRement enable
        hwbyte_t                    :3;   // 00.1-3
        hwbyte_t CSR_AWUEN          :1;   // 00.4   // Auto-Wake-Up ENable
        hwbyte_t CSR_AWUF           :1;   // 00.5   // Auto-Wake-Up interrupt Flag
        hwbyte_t                    :2;   // 00.6-7
        hwbyte_t APR_APR            :6;   // 01.0-5 // Asynchronous PRescaler
        hwbyte_t                    :2;   // 01.6-7
        hwbyte_t TBR_AWUTB          :4;   // 02.0-3 // TimeBase selection
        hwbyte_t                    :4;   // 01.4-7
    };
    struct {
        hwbyte_t CSR;                     // 00     // Control Status Reg
        hwbyte_t APR;                     // 01     // Asynchronous Prescaler Reg
        hwbyte_t TBR;                     // 02     // TimeBase selection Reg
    };
} AWU_t;
AWU_t AWU                           @0x50F0;


// BEEP section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CSR_BEEPDIV        :5;   // 00.0-4 // BEEP Divider
        hwbyte_t CSR_BEEPEN         :1;   // 00.5   // BEEP ENable
        hwbyte_t CSR_BEEPSEL        :2;   // 00.6-7 // BEEP frequency SELection
    };
    struct {
        hwbyte_t CSR;                     // 00     // Control Status Reg
    };
} BEEP_t;
BEEP_t BEEP                         @0x50F3;


//  SPI section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_CPHA           :1;   // 00.0   // Clock PHAse
        hwbyte_t CR1_CPOL           :1;   // 00.1   // Clock POLarity
        hwbyte_t CR1_MSTR           :1;   // 00.2   // MaSTer selection
        hwbyte_t CR1_BR             :3;   // 00.3-5 // Baud Rate control
        hwbyte_t CR1_SPE            :1;   // 00.6   // SPI Enable
        hwbyte_t CR1_LSBFIRST       :1;   // 00.7   // frame format
        hwbyte_t CR2_SSI            :1;   // 01.0   // Internal Slave Select
        hwbyte_t CR2_SSM            :1;   // 01.1   // Software Slave Management
        hwbyte_t CR2_RXONLY         :1;   // 01.2   // RX ONLY
        hwbyte_t                    :1;   // 01.3
        hwbyte_t CR2_CRCNEXT        :1;   // 01.4   // Transmit CRC NEXT
        hwbyte_t CR2_CRCEN          :1;   // 01.5   // CRC calculation ENable
        hwbyte_t CR2_BDOE           :1;   // 01.6   // Bi-Directional mode Output Enable
        hwbyte_t CR2_BDM            :1;   // 01.7   // Bi-Directional mode enable
        hwbyte_t                    :4;   // 02.0-3
        hwbyte_t ICR_WKIE           :1;   // 02.4   // Transmit CRC NEXT
        hwbyte_t ICR_ERRIE          :1;   // 02.5   // CRC calculation ENable
        hwbyte_t ICR_RXEI           :1;   // 02.6   // Bi-Directional mode Output Enable
        hwbyte_t ICR_TXEI           :1;   // 02.7   // Bi-Directional mode enable
        hwbyte_t SR_RXNE            :1;   // 03.0   // RX buffer Not Empty
        hwbyte_t SR_TXE             :1;   // 03.1   // TX buffer Empty
        hwbyte_t SR_WKUP            :1;   // 03.2   // WaKe-up flag
        hwbyte_t                    :1;   // 03.3
        hwbyte_t SR_CRCERR          :1;   // 03.4   // CRC ERRor flag
        hwbyte_t SR_MODF            :1;   // 03.5   // MODe Fault
        hwbyte_t SR_OVR             :1;   // 03.6   // OVerRun flag
        hwbyte_t SR_BSY             :1;   // 03.7   // BuSY flag
        hwbyte_t                    :8;   // 04
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Reg 1
        hwbyte_t CR2;                     // 01     // Control Reg 2
        hwbyte_t ICR;                     // 02     // Interrupt Control Reg
        hwbyte_t SR;                      // 03     // Status Reg
        hwbyte_t DR;                      // 04     // Data Reg
        hwbyte_t CRCPR;                   // 05     // CRC Polynomial Reg
        hwbyte_t RXCRCR;                  // 06     // RX CRC Reg
        hwbyte_t TXCRCR;                  // 07     // TX CRC Reg
    };
} SPI_t;
SPI_t SPI                           @0x5200;
SPI_t SPI1                          @0x5200;


// I2C section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_PE             :1;   // 00.0   // Peripheral Enable
        hwbyte_t                    :5;   // 00.1-5
        hwbyte_t CR1_ENGC           :1;   // 00.6   // ENable General Call
        hwbyte_t CR1_NOSTRETCH      :1;   // 00.7   // NO clock STRETCHing in slave mode
        hwbyte_t CR2_START          :1;   // 01.0   // START generation
        hwbyte_t CR2_STOP           :1;   // 01.1   // STOP generation
        hwbyte_t CR2_ACK            :1;   // 01.2   // ACKnowledge enable
        hwbyte_t CR2_POS            :1;   // 01.3   // acknowledge POSition
        hwbyte_t                    :3;   // 01.4-6
        hwbyte_t CR2_SWRST          :1;   // 01.7   // SoftWare RESet
        hwbyte_t FREQR_FREQ         :6;   // 02.0-5 // peripheral clock FREQuency
        hwbyte_t                    :2;   // 02.6-7
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :1;   // 04.0
        hwbyte_t OARH_ADD           :2;   // 04.1-2 // ADDress bits [9..8] in 10-bit addressing mode
        hwbyte_t                    :3;   // 04.3-5
        hwbyte_t OARH_ADDCONF       :1;   // 04.6   // ADDress mode CONFiguration
        hwbyte_t OARH_ADDMODE       :1;   // 04.7   // ADDressing MODE in slave mode
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t SR1_SB             :1;   // 07.0   // Start Bit in master mode
        hwbyte_t SR1_ADDR           :1;   // 07.1   // ADDRess sent in master mode, matched in slave mode
        hwbyte_t SR1_BTF            :1;   // 07.2   // Byte Transfer Finished
        hwbyte_t SR1_ADD10          :1;   // 07.3   // 10-bit ADDress header sent in master mode
        hwbyte_t SR1_STOPF          :1;   // 07.4   // STOP detected Flag
        hwbyte_t                    :1;   // 07.5
        hwbyte_t SR1_RXNE           :1;   // 07.6   // Receive Data Reg Not Empty
        hwbyte_t SR1_TXE            :1;   // 07.7   // Transmit Data Register Empty
        hwbyte_t SR2_BERR           :1;   // 08.0   // Bus ERRor
        hwbyte_t SR2_ARLO           :1;   // 08.1   // ARbitration LOst in master mode
        hwbyte_t SR2_AF             :1;   // 08.2   // Acknowledge Failure
        hwbyte_t SR2_OVR            :1;   // 08.3   // OVerRun or underrun
        hwbyte_t                    :1;   // 08.4
        hwbyte_t SR2_WUFH           :1;   // 08.5   // Wake-Up From Halt
        hwbyte_t                    :2;   // 08.6-7
        hwbyte_t SR3_MSL            :1;   // 09.0   // Master/SLave
        hwbyte_t SR3_BUSY           :1;   // 09.1   // bus BUSY
        hwbyte_t SR3_TRA            :1;   // 09.2   // TRAnsmitter/receiver
        hwbyte_t                    :1;   // 09.3
        hwbyte_t SR3_GENCALL        :1;   // 09.4   // GENeral CALL Header received in slave mode
        hwbyte_t                    :3;   // 09.5-7
        hwbyte_t ITR_ITERREN        :1;   // 0A.0   // ERRor InTerrupt ENable
        hwbyte_t ITR_ITEVTEN        :1;   // 0A.1   // EVenT InTerrupt ENable
        hwbyte_t ITR_ITBUFEN        :1;   // 0A.2   // BUFfer InTerrupt ENable
        hwbyte_t                    :5;   // 0A.3-7
        hwbyte_t                    :8;   // 0B
        hwbyte_t CCRH_CCR           :4;   // 0C.0-3 // Clock Control Reg bits [11..8]
        hwbyte_t                    :2;   // 0C.4-5
        hwbyte_t CCRH_DUTY          :1;   // 0C.6   // fast mode DUTY cycle
        hwbyte_t CCRH_FS            :1;   // 0C.7   // Fast/Slow mode
        hwbyte_t TRISER_TRISE       :6;   // 0D.0-5 // maximum RISE Time
        hwbyte_t                    :2;   // 0D.6-7
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Reg 1
        hwbyte_t CR2;                     // 01     // Control Reg 2
        hwbyte_t FREQR;                   // 02     // FREQuency Reg
        hwbyte_t OARL;                    // 03     // Own Address Reg Low
        hwbyte_t OARH;                    // 04     // Own Address Reg High
        hwbyte_t                    :8;   // 05
        hwbyte_t DR;                      // 06     // Data Reg
        hwbyte_t SR1;                     // 07     // Status Reg 1
        hwbyte_t SR2;                     // 08     // Status Reg 2
        hwbyte_t SR3;                     // 09     // Status Reg 3
        hwbyte_t ITR;                     // 0A     // InTerrupt Reg
        hwbyte_t CCRL;                    // 0B     // Clock Control Reg Low
        hwbyte_t CCRH;                    // 0C     // Clock Control Reg High
        hwbyte_t TRISER;                  // 0D     // maximum RISE Time Reg
    };
} I2C_t;
I2C_t I2C                           @0x5210;
I2C_t I2C1                          @0x5210;


// UART section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t SR_PE              :1;   // 00.0   // Parity Error
        hwbyte_t SR_FE              :1;   // 00.1   // Framing Error
        hwbyte_t SR_NF              :1;   // 00.2   // Noise Flag
        hwbyte_t SR_OR              :1;   // 00.3   // OverRun error / LIN Header Error
        hwbyte_t SR_IDLE            :1;   // 00.4   // IDLE line detected
        hwbyte_t SR_RXNE            :1;   // 00.5   // Receive data Reg Not Empty
        hwbyte_t SR_TC              :1;   // 00.6   // Transmission Complete
        hwbyte_t SR_TXE             :1;   // 00.7   // Transmit data Reg Empty
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t CR1_PIEN           :1;   // 04.0   // Parity error Interrupt ENable
        hwbyte_t CR1_PS             :1;   // 04.1   // Parity Selection
        hwbyte_t CR1_PCEN           :1;   // 04.2   // Parity Control ENable
        hwbyte_t CR1_WAKE           :1;   // 04.3   // WAKE-up method
        hwbyte_t CR1_M              :1;   // 04.4   // word length
        hwbyte_t CR1_UARTD          :1;   // 04.5   // UART Disable
        hwbyte_t CR1_T8             :1;   // 04.6   // Transmit data bit 8
        hwbyte_t CR1_R8             :1;   // 04.7   // Receive data bit 8
        hwbyte_t CR2_SBK            :1;   // 05.0   // Send BreaK
        hwbyte_t CR2_RWU            :1;   // 05.1   // Receiver Wake-Up
        hwbyte_t CR2_REN            :1;   // 05.2   // Receiver ENable
        hwbyte_t CR2_TEN            :1;   // 05.3   // Transmitter ENable
        hwbyte_t CR2_ILIEN          :1;   // 05.4   // Idle Line Interrupt ENable
        hwbyte_t CR2_RIEN           :1;   // 05.5   // Receiver Interrupt ENable
        hwbyte_t CR2_TCIEN          :1;   // 05.6   // Transmission Complete Interrupt ENable
        hwbyte_t CR2_TIEN           :1;   // 05.7   // Transmitter Interrupt ENable
        hwbyte_t CR3_LBCL           :1;   // 06.0   // Last Bit CLock pulse
        hwbyte_t CR3_CPHA           :1;   // 06.1   // Clock PHAse
        hwbyte_t CR3_CPOL           :1;   // 06.2   // Clock POLarity
        hwbyte_t CR3_CKEN           :1;   // 06.3   // ClocK ENable
        hwbyte_t CR3_STOP           :2;   // 06.4-5 // STOP bits
        hwbyte_t CR3_LINEN          :1;   // 06.6   // LIN mode ENable
        hwbyte_t                    :1;   // 06.7
        hwbyte_t CR4_ADD            :4;   // 07.0-3 // ADDress of the node
        hwbyte_t CR4_LBDF           :1;   // 07.4   // LIN Break Detection Flag
        hwbyte_t CR4_LBDL           :1;   // 07.5   // LIN Break Detection Length
        hwbyte_t CR4_LBDIEN         :1;   // 07.6   // LIN Break Detection Interrupt ENable
        hwbyte_t                    :1;   // 07.7
        hwbyte_t                    :1;   // 08.0
        hwbyte_t CR5_IREN           :1;   // 08.1   // IRda ENable
        hwbyte_t CR5_IRLP           :1;   // 08.2   // IRda Low Power selection
        hwbyte_t CR5_HDSEL          :1;   // 08.3   // Half-Duplex SELection
        hwbyte_t CR5_NACK           :1;   // 08.4   // smart card NACK ENable
        hwbyte_t CR5_SCEN           :1;   // 08.5   // Smart Card mode ENable
        hwbyte_t                    :2;   // 08.6-7
        hwbyte_t                    :8;   // 09
        hwbyte_t                    :8;   // 0A
    };
    struct {
        hwbyte_t SR;                      // 00     // Status Reg
        hwbyte_t DR;                      // 01     // Data Reg
        hwbyte_t BRR1;                    // 02     // Baud Rate Reg 1
        hwbyte_t BRR2;                    // 03     // Baud Rate reg 2
        hwbyte_t CR1;                     // 04     // Control Reg 1
        hwbyte_t CR2;                     // 05     // Control Reg 2
        hwbyte_t CR3;                     // 06     // Control Reg 3
        hwbyte_t CR4;                     // 07     // Control Reg 4
        hwbyte_t CR5;                     // 08     // Control Reg 5
        hwbyte_t GTR;                     // 09     // Guard Time Reg
        hwbyte_t PSCR;                    // 0A     // Prescaler Reg
    };
} UART_t;
UART_t UART                         @0x5230;
UART_t UART1                        @0x5230;


// TIMER 1 section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_CEN            :1;   // 00.0   // Counter Enable
        hwbyte_t CR1_UDIS           :1;   // 00.1   // Update DIsable
        hwbyte_t CR1_URS            :1;   // 00.2   // Update Request Source
        hwbyte_t CR1_OPM            :1;   // 00.3   // One Pulse Mode
        hwbyte_t CR1_DIR            :1;   // 00.4   // Direction
        hwbyte_t CR1_CMS            :2;   // 00.5-6 // Center-aligned Mode Selection
        hwbyte_t CR1_ARPE           :1;   // 00.7   // Auto-Reload Preload Enable
        hwbyte_t CR2_CCPC           :1;   // 01.0   // Capture/Compare Preloaded Control
        hwbyte_t                    :1;   // 01.1
        hwbyte_t CR2_COMS           :1;   // 01.2   // capture/COMpare control update Selection
        hwbyte_t                    :1;   // 01.3
        hwbyte_t CR2_MMS            :3;   // 01.4-6 // Master Mode Selection
        hwbyte_t                    :1;   // 01.7
        hwbyte_t SMCR_SMS           :3;   // 02.0-2 // Slave Mode Selection
        hwbyte_t                    :1;   // 02.3
        hwbyte_t SMCR_TS            :3;   // 02.4-6 // Trigger Selection
        hwbyte_t SMCR_MSM           :1;   // 02.7   // Master/Slave Mode
        hwbyte_t ETR_ETF            :4;   // 03.0-3 // External Trigger Filter
        hwbyte_t ETR_ETPS           :2;   // 03.4-5 // External Trigger PreScaler
        hwbyte_t ETR_ECE            :1;   // 03.6   // External Clock Enable
        hwbyte_t ETR_ETP            :1;   // 03.7   // External Trigger Polarity
        hwbyte_t IER_UIE            :1;   // 04.0   // Update Interrupt Enable
        hwbyte_t IER_CC1IE          :1;   // 04.1   // Capture/Compare 1 Interrupt Enable
        hwbyte_t IER_CC2IE          :1;   // 04.2   // Capture/Compare 2 Interrupt Enable
        hwbyte_t IER_CC3IE          :1;   // 04.3   // Capture/Compare 3 Interrupt Enable
        hwbyte_t IER_CC4IE          :1;   // 04.4   // Capture/Compare 4 Interrupt Enable
        hwbyte_t IER_COMIE          :1;   // 04.5   // COMmutation Interrupt Enable
        hwbyte_t IER_TIE            :1;   // 04.6   // Trigger Interrupt Enable
        hwbyte_t IER_BIE            :1;   // 04.7   // Break Interrupt Enable
        hwbyte_t SR1_UIF            :1;   // 05.0   // Update Interrupt Flag
        hwbyte_t SR1_CC1IF          :1;   // 05.1   // Capture/Compare 1 Interrupt Flag
        hwbyte_t SR1_CC2IF          :1;   // 05.2   // Capture/Compare 2 Interrupt Flag
        hwbyte_t SR1_CC3IF          :1;   // 05.3   // Capture/Compare 3 Interrupt Flag
        hwbyte_t SR1_CC4IF          :1;   // 05.4   // Capture/Compare 4 Interrupt Flag
        hwbyte_t SR1_COMIF          :1;   // 05.5   // Commutation Interrupt Flag
        hwbyte_t SR1_TIF            :1;   // 05.6   // Trigger Interrupt Flag
        hwbyte_t SR1_BIF            :1;   // 05.7   // Break Interrupt Flag
        hwbyte_t                    :1;   // 06.0
        hwbyte_t SR2_CC1OF          :1;   // 06.1   // Capture/Compare 1 Overcapture Flag
        hwbyte_t SR2_CC2OF          :1;   // 06.2   // Capture/Compare 2 Overcapture Flag
        hwbyte_t SR2_CC3OF          :1;   // 06.3   // Capture/Compare 3 Overcapture Flag
        hwbyte_t SR2_CC4OF          :1;   // 06.4   // Capture/Compare 4 Overcapture Flag
        hwbyte_t                    :3;   // 06.5-7
        hwbyte_t EGR_UG             :1;   // 07.0   // Update Generation
        hwbyte_t EGR_CC1G           :1;   // 07.1   // Capture/Compare 1 Generation
        hwbyte_t EGR_CC2G           :1;   // 07.2   // Capture/Compare 2 Generation
        hwbyte_t EGR_CC3G           :1;   // 07.3   // Capture/Compare 3 Generation
        hwbyte_t EGR_CC4G           :1;   // 07.4   // Capture/Compare 4 Generation
        hwbyte_t EGR_COMG           :1;   // 07.5   // Capture/Compare Control Update Generation
        hwbyte_t EGR_TG             :1;   // 07.6   // Trigger Generation
        hwbyte_t EGR_BG             :1;   // 07.7   // Break Generation
        hwbyte_t CCMR1_CC1S         :2;   // 08.0-1 // Capture/Compare 1 Selection
        hwbyte_t CCMR1_OC1FE        :1;   // 08.2   // Output Compare 1 Fast Enable
        hwbyte_t CCMR1_OC1PE        :1;   // 08.3   // Output Compare 1 Preload Enable
        hwbyte_t CCMR1_OC1M         :3;   // 08.4-6 // Output Compare 1 Mode
        hwbyte_t CCMR1_OC1CE        :1;   // 08.7   // Output Compare 1 Clear Enable
        hwbyte_t CCMR2_CC1S         :2;   // 09.0-1 // Capture/Compare 2 Selection
        hwbyte_t CCMR2_OC1FE        :1;   // 09.2   // Output Compare 2 Fast Enable
        hwbyte_t CCMR2_OC1PE        :1;   // 09.3   // Output Compare 2 Preload Enable
        hwbyte_t CCMR2_OC1M         :3;   // 09.4-6 // Output Compare 2 Mode
        hwbyte_t CCMR2_OC1CE        :1;   // 09.7   // Output Compare 2 Clear Enable
        hwbyte_t CCMR3_CC1S         :2;   // 0A.0-1 // Capture/Compare 3 Selection
        hwbyte_t CCMR3_OC1FE        :1;   // 0A.2   // Output Compare 3 Fast Enable
        hwbyte_t CCMR3_OC1PE        :1;   // 0A.3   // Output Compare 3 Preload Enable
        hwbyte_t CCMR3_OC1M         :3;   // 0A.4-6 // Output Compare 3 Mode
        hwbyte_t CCMR3_OC1CE        :1;   // 0A.7   // Output Compare 3 Clear Enable
        hwbyte_t CCMR4_CC1S         :2;   // 0B.0-1 // Capture/Compare 4 Selection
        hwbyte_t CCMR4_OC1FE        :1;   // 0B.2   // Output Compare 4 Fast Enable
        hwbyte_t CCMR4_OC1PE        :1;   // 0B.3   // Output Compare 4 Preload Enable
        hwbyte_t CCMR4_OC1M         :3;   // 0B.4-6 // Output Compare 4 Mode
        hwbyte_t CCMR4_OC1CE        :1;   // 0B.7   // Output Compare 4 Clear Enable
        hwbyte_t CCER1_CC1E         :1;   // 0C.0   // Capture/Compare 1 output Enable
        hwbyte_t CCER1_CC1P         :1;   // 0C.1   // Capture/Compare 1 output Polarity
        hwbyte_t CCER1_CC1NE        :1;   // 0C.2   // Capture/Compare 1 Complementary output Enable
        hwbyte_t CCER1_CC1NP        :1;   // 0C.3   // Capture/Compare 1 Complementary output Polarity
        hwbyte_t CCER1_CC2E         :1;   // 0C.4   // Capture/Compare 2 output Enable
        hwbyte_t CCER1_CC2P         :1;   // 0C.5   // Capture/Compare 2 output Polarity
        hwbyte_t CCER1_CC2NE        :1;   // 0C.6   // Capture/Compare 2 Complementary output Enable
        hwbyte_t CCER1_CC2NP        :1;   // 0C.7   // Capture/Compare 2 Complementary output Polarity
        hwbyte_t CCER2_CC3E         :1;   // 0D.0   // Capture/Compare 3 output Enable
        hwbyte_t CCER2_CC3P         :1;   // 0D.1   // Capture/Compare 3 output Polarity
        hwbyte_t CCER2_CC3NE        :1;   // 0D.2   // Capture/Compare 3 Complementary output Enable
        hwbyte_t CCER2_CC3NP        :1;   // 0D.3   // Capture/Compare 3 Complementary output Polarity
        hwbyte_t CCER2_CC4E         :1;   // 0D.4   // Capture/Compare 4 output Enable
        hwbyte_t CCER2_CC4P         :1;   // 0D.5   // Capture/Compare 4 output Polarity
        hwbyte_t                    :2;   // 0D.6-7
        hwword_t CNTR;                    // 0E-0F  // CouNTer Reg
        hwword_t PSCR;                    // 10-11  // CouNTer Reg
        hwword_t ARR;                     // 12-13  // Auto-Reload Reg
        hwbyte_t                    :8;   // 14
        hwword_t CCR1;                    // 15-16  // Capture/Compare Reg 1
        hwword_t CCR2;                    // 17-18  // Capture/Compare Reg 2
        hwword_t CCR3;                    // 19-1A  // Capture/Compare Reg 3
        hwword_t CCR4;                    // 1B-1C  // Capture/Compare Reg 4
        hwbyte_t BKR_LOCK           :2;   // 1D.0   // LOCK Configuration
        hwbyte_t BKR_OSSI           :1;   // 1D.2   // Off-State Selection for Idle mode
        hwbyte_t BKR_OSSR           :1;   // 1D.3   // Off-State Selection for Run mode
        hwbyte_t BKR_BKE            :1;   // 1D.4   // BreaK Enable
        hwbyte_t BKR_BKP            :1;   // 1D.5   // BreaK Polarity
        hwbyte_t BKR_AOE            :1;   // 1D.6   // Automatic Output Enable
        hwbyte_t BKR_MOE            :1;   // 1D.7   // Main Output Enable
        hwbyte_t                    :8;   // 1E
        hwbyte_t OISR_OIS1          :1;   // 1F.0   // Output Idle state 1 (OC1 output)
        hwbyte_t OISR_OIS1N         :1;   // 1F.1   // Output Idle state 1 (OC1N output)
        hwbyte_t OISR_OIS2          :1;   // 1F.2   // Output Idle state 2 (OC2 output)
        hwbyte_t OISR_OIS2N         :1;   // 1F.3   // Output Idle state 2 (OC2N output)
        hwbyte_t OISR_OIS3          :1;   // 1F.4   // Output Idle state 3 (OC3 output)
        hwbyte_t OISR_OIS3N         :1;   // 1F.5   // Output Idle state 3 (OC3N output)
        hwbyte_t OISR_OIS4          :1;   // 1F.6   // Output Idle state 4 (OC4 output)
        hwbyte_t                    :1;   // 1F.7
    };
    struct {
        hwbyte_t                    :8;   // 00
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
        hwbyte_t                    :2;   // 08.0-1
        hwbyte_t CCMR1_IC1PSC       :2;   // 08.2-3 // Input Capture 1 PreSCaler
        hwbyte_t CCMR1_IC1F         :4;   // 08.4-7 // Input Capture 1 Filter
        hwbyte_t                    :2;   // 09.0-1
        hwbyte_t CCMR2_IC2PSC       :2;   // 09.2-3 // Input Capture 2 PreSCaler
        hwbyte_t CCMR2_IC2F         :4;   // 09.4-7 // Input Capture 2 Filter
        hwbyte_t                    :2;   // 0A.0-1
        hwbyte_t CCMR3_IC3PSC       :2;   // 0A.2-3 // Input Capture 3 PreSCaler
        hwbyte_t CCMR3_IC3F         :4;   // 0A.4-7 // Input Capture 3 Filter
        hwbyte_t                    :2;   // 0B.0-1
        hwbyte_t CCMR4_IC3PSC       :2;   // 0B.2-3 // Input Capture 3 PreSCaler
        hwbyte_t CCMR4_IC3F         :4;   // 0B.4-7 // Input Capture 3 Filter
        hwbyte_t                    :8;   // 0C
        hwbyte_t                    :8;   // 0D
        hwword_t                    :16;  // 0E-0F
        hwword_t                    :16;  // 10-11
        hwword_t                    :16;  // 12-13
        hwbyte_t                    :8;   // 14
        hwword_t                    :16;  // 15-16
        hwword_t                    :16;  // 17-18
        hwword_t                    :16;  // 19-1A
        hwword_t                    :16;  // 1B-1C
        hwbyte_t                    :8;   // 1D
        hwbyte_t                    :8;   // 1E
        hwbyte_t                    :8;   // 1F
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Reg 1
        hwbyte_t CR2;                     // 01     // Control Reg 2
        hwbyte_t SMCR;                    // 02     // Slave Mode Control Reg
        hwbyte_t ETR;                     // 03     // External Trigger Reg
        hwbyte_t IER;                     // 04     // Interrupt Enable Reg
        hwbyte_t SR1;                     // 05     // Status Reg 1
        hwbyte_t SR2;                     // 06     // Status Reg 2
        hwbyte_t EGR;                     // 07     // Event Generation Reg
        hwbyte_t CCMR1;                   // 08     // Capture/Compare Mode Reg 1
        hwbyte_t CCMR2;                   // 09     // Capture/Compare Mode Reg 2
        hwbyte_t CCMR3;                   // 0A     // Capture/Compare Mode Reg 3
        hwbyte_t CCMR4;                   // 0B     // Capture/Compare Mode Reg 4
        hwbyte_t CCER1;                   // 0C     // Capture/Compare Enable Reg 1
        hwbyte_t CCER2;                   // 0D     // Capture/Compare Enable Reg 2
        hwbyte_t CNTRH;                   // 0E     // CouNTer Reg High
        hwbyte_t CNTRL;                   // 0F     // CouNTer Reg Low
        hwbyte_t PSCRH;                   // 10     // PreSCaler Reg High
        hwbyte_t PSCRL;                   // 11     // PreSCaler Reg Low
        hwbyte_t ARRH;                    // 12     // Auto-Reload Reg High
        hwbyte_t ARRL;                    // 13     // Auto-Reload Reg Low
        hwbyte_t RCR;                     // 14     // Repetition Counter Reg
        hwbyte_t CCR1H;                   // 14     // Capture/Compare Reg 1 High
        hwbyte_t CCR1L;                   // 15     // Capture/Compare Reg 1 Low
        hwbyte_t CCR2H;                   // 16     // Capture/Compare Reg 2 High
        hwbyte_t CCR2L;                   // 17     // Capture/Compare Reg 2 Low
        hwbyte_t CCR3H;                   // 18     // Capture/Compare Reg 3 High
        hwbyte_t CCR3L;                   // 19     // Capture/Compare Reg 3 Low
        hwbyte_t CCR4H;                   // 18     // Capture/Compare Reg 4 High
        hwbyte_t CCR4L;                   // 19     // Capture/Compare Reg 4 Low
        hwbyte_t BKR;                     // 18     // BreaK Reg
        hwbyte_t DTR;                     // 19     // Dead Time Reg
        hwbyte_t OISR;                    // 18     // Output Idle State Reg
    };
} TIM1_t;
TIM1_t TIM1                         @0x5250;


// TIMER 2 section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_CEN            :1;   // 00.0   // Counter Enable
        hwbyte_t CR1_UDIS           :1;   // 00.1   // Update DIsable
        hwbyte_t CR1_URS            :1;   // 00.2   // Update Request Source
        hwbyte_t CR1_OPM            :1;   // 00.3   // One Pulse Mode
        hwbyte_t                    :3;   // 00.4-6
        hwbyte_t CR1_ARPE           :1;   // 00.7   // Auto-Reload Preload Enable
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t IER_UIE            :1;   // 03.0   // Update Interrupt Enable
        hwbyte_t IER_CC1IE          :1;   // 03.1   // Capture/Compare 1 Interrupt Enable
        hwbyte_t IER_CC2IE          :1;   // 03.2   // Capture/Compare 2 Interrupt Enable
        hwbyte_t IER_CC3IE          :1;   // 03.3   // Capture/Compare 3 Interrupt Enable
        hwbyte_t                    :4;   // 03.4-7
        hwbyte_t SR1_UIF            :1;   // 04.0   // Update Interrupt Flag
        hwbyte_t SR1_CC1IF          :1;   // 04.1   // Capture/Compare 1 Interrupt Flag
        hwbyte_t SR1_CC2IF          :1;   // 04.2   // Capture/Compare 2 Interrupt Flag
        hwbyte_t SR1_CC3IF          :1;   // 04.3   // Capture/Compare 3 Interrupt Flag
        hwbyte_t                    :4;   // 04.4-7
        hwbyte_t                    :1;   // 05.0
        hwbyte_t SR2_CC1OF          :1;   // 05.1   // Capture/Compare 1 Overcapture Flag
        hwbyte_t SR2_CC2OF          :1;   // 05.2   // Capture/Compare 2 Overcapture Flag
        hwbyte_t SR2_CC3OF          :1;   // 05.3   // Capture/Compare 3 Overcapture Flag
        hwbyte_t                    :4;   // 05.4-7
        hwbyte_t EGR_UG             :1;   // 06.0   // Update Generation
        hwbyte_t EGR_CC1G           :1;   // 06.1   // Capture/Compare 1 Generation
        hwbyte_t EGR_CC2G           :1;   // 06.2   // Capture/Compare 2 Generation
        hwbyte_t EGR_CC3G           :1;   // 06.3   // Capture/Compare 3 Generation
        hwbyte_t                    :4;   // 06.4-7
        hwbyte_t CCMR1_CC1S         :2;   // 07.0-1 // Capture/Compare 1 Selection
        hwbyte_t                    :1;   // 07.2
        hwbyte_t CCMR1_OC1PE        :1;   // 07.3   // Output Compare 1 Preload Enable
        hwbyte_t CCMR1_OC1M         :3;   // 07.4-6 // Output Compare 1 Mode
        hwbyte_t                    :1;   // 07.7
        hwbyte_t CCMR2_CC2S         :2;   // 08.0-1 // Capture/Compare 1 Selection
        hwbyte_t                    :1;   // 08.2
        hwbyte_t CCMR2_OC2PE        :1;   // 08.3   // Output Compare 1 Preload Enable
        hwbyte_t CCMR2_OC2M         :3;   // 08.4-6 // Output Compare 1 Mode
        hwbyte_t                    :1;   // 08.7
        hwbyte_t CCMR3_CC3S         :2;   // 09.0-1 // Capture/Compare 1 Selection
        hwbyte_t                    :1;   // 09.2
        hwbyte_t CCMR3_OC3PE        :1;   // 09.3   // Output Compare 1 Preload Enable
        hwbyte_t CCMR3_OC3M         :3;   // 09.4-6 // Output Compare 1 Mode
        hwbyte_t                    :1;   // 09.7
        hwbyte_t CCER1_CC1E         :1;   // 0A.0   // Capture/Compare 1 output Enable
        hwbyte_t CCER1_CC1P         :1;   // 0A.1   // Capture/Compare 1 output Polarity
        hwbyte_t                    :2;   // 0A.2-3
        hwbyte_t CCER1_CC2E         :1;   // 0A.4   // Capture/Compare 2 output Enable
        hwbyte_t CCER1_CC2P         :1;   // 0A.5   // Capture/Compare 2 output Polarity
        hwbyte_t                    :2;   // 0A.6-7
        hwbyte_t CCER2_CC3E         :1;   // 0B.0   // Capture/Compare 3 output Enable
        hwbyte_t CCER2_CC3P         :1;   // 0B.1   // Capture/Compare 3 output Polarity
        hwbyte_t                    :6;   // 0B.2-7
        hwword_t CNTR;                    // 0C-0D  // CouNTer Reg
        hwbyte_t                    :8;   // 0E
        hwword_t ARR;                     // 0F-10  // Auto-Reload Reg
        hwword_t CCR1;                    // 11-12  // Capture/Compare Reg 1
        hwword_t CCR2;                    // 13-14  // Capture/Compare Reg 2
        hwword_t CCR3;                    // 15-16  // Capture/Compare Reg 3
    };
    struct {
        hwbyte_t                    :8;   // 00
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :2;   // 07.0-1
        hwbyte_t CCMR1_IC1PSC       :2;   // 07.2-3 // Input Capture 1 PreSCaler
        hwbyte_t CCMR1_IC1F         :4;   // 07.4-7 // Input Capture 1 Filter
        hwbyte_t                    :2;   // 08.0-1
        hwbyte_t CCMR2_IC2PSC       :2;   // 08.2-3 // Input Capture 2 PreSCaler
        hwbyte_t CCMR2_IC2F         :4;   // 08.4-7 // Input Capture 2 Filter
        hwbyte_t                    :2;   // 09.0-1
        hwbyte_t CCMR3_IC3PSC       :2;   // 09.2-3 // Input Capture 3 PreSCaler
        hwbyte_t CCMR3_IC3F         :4;   // 09.4-7 // Input Capture 3 Filter
        hwbyte_t                    :8;   // 0A
        hwbyte_t                    :8;   // 0B
        hwword_t                    :16;  // 0C-0D
        hwbyte_t                    :8;   // 0E
        hwword_t                    :16;  // 0F-10
        hwword_t                    :16;  // 11-12
        hwword_t                    :16;  // 13-14
        hwword_t                    :16;  // 15-16
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Reg 1
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t IER;                     // 03     // Interrupt Enable Reg
        hwbyte_t SR1;                     // 04     // Status Reg 1
        hwbyte_t SR2;                     // 05     // Status Reg 2
        hwbyte_t EGR;                     // 06     // Event Generation Reg
        hwbyte_t CCMR1;                   // 07     // Capture/Compare Mode Reg 1
        hwbyte_t CCMR2;                   // 08     // Capture/Compare Mode Reg 2
        hwbyte_t CCMR3;                   // 09     // Capture/Compare Mode Reg 3
        hwbyte_t CCER1;                   // 0A     // Capture/Compare Enable Reg 1
        hwbyte_t CCER2;                   // 0B     // Capture/Compare Enable Reg 2
        hwbyte_t CNTRH;                   // 0C     // CouNTer Reg High
        hwbyte_t CNTRL;                   // 0D     // CouNTer Reg Low
        hwbyte_t PSCR;                    // 0E     // PreSCaler Reg
        hwbyte_t ARRH;                    // 0F     // Auto-Reload Reg High
        hwbyte_t ARRL;                    // 10     // Auto-Reload Reg Low
        hwbyte_t CCR1H;                   // 11     // Capture/Compare Reg 1 High
        hwbyte_t CCR1L;                   // 12     // Capture/Compare Reg 1 Low
        hwbyte_t CCR2H;                   // 13     // Capture/Compare Reg 2 High
        hwbyte_t CCR2L;                   // 14     // Capture/Compare Reg 2 Low
        hwbyte_t CCR3H;                   // 15     // Capture/Compare Reg 3 High
        hwbyte_t CCR3L;                   // 16     // Capture/Compare Reg 3 Low
    };
} TIM2_t;
TIM2_t TIM2                         @0x5300;        // TIMER 2


//  TIMER 4 section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_CEN            :1;   // 00.0   // Counter Enable
        hwbyte_t CR1_UDIS           :1;   // 00.1   // Update DIsable
        hwbyte_t CR1_URS            :1;   // 00.2   // Update Request Source
        hwbyte_t CR1_OPM            :1;   // 00.3   // One Pulse Mode
        hwbyte_t                    :3;   // 00.4-6
        hwbyte_t CR1_ARPE           :1;   // 00.7   // Auto-Reload Preload Enable
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t IER_UIE            :1;   // 03.0   // Update Interrupt Enable
        hwbyte_t                    :7;   // 03.1-7
        hwbyte_t SR_UIF             :1;   // 04.0   // Update Interrupt Flag
        hwbyte_t                    :7;   // 04.1-7
        hwbyte_t EGR_UG             :1;   // 05.0   // Update Generation
        hwbyte_t                    :7;   // 05.1-7
        hwbyte_t                    :8;   // 06
        hwbyte_t PSCR_PSC           :3;   // 07.0-2 // PreSCaler value
        hwbyte_t                    :5;   // 07.3-7
        hwbyte_t                    :8;   // 08
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Reg 1
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t IER;                     // 03     // Interrupt Enable Reg
        hwbyte_t SR;                      // 04     // Status Reg
        hwbyte_t EGR;                     // 05     // Event Generation Reg
        hwbyte_t CNTR;                    // 06     // CouNTer Reg
        hwbyte_t PSCR;                    // 07     // PreSCaler Reg
        hwbyte_t ARR;                     // 08     // Auto-Reload Reg
    };
} TIM4_t;
TIM4_t TIM4 @0x5340;                                // TIMER 4


//  ADC section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwword_t DBR[10];                 // 00-13  // Data Buffer Regs
        hwword_t                    :16;  // 14-15
        hwword_t                    :16;  // 16-17
        hwword_t                    :16;  // 18-19
        hwword_t                    :16;  // 1A-1B
        hwword_t                    :16;  // 1C-1D
        hwword_t                    :16;  // 1E-1F
        hwbyte_t CSR_CH             :4;   // 20.0-3 // CHannel selection
        hwbyte_t CSR_AWDIE          :1;   // 20.4   // Analog WatchDog Interrupt Enable
        hwbyte_t CSR_EOCIE          :1;   // 20.5   // End Of Conversion Interrupt Enable
        hwbyte_t CSR_AWD            :1;   // 20.6   // Analog WatchDog status
        hwbyte_t CSR_EOC            :1;   // 20.7   // End Of Conversion
        hwbyte_t CR1_ADON           :1;   // 21.0   // A/D converter On
        hwbyte_t CR1_CONT           :1;   // 21.1   // CONTinuous conversion
        hwbyte_t                    :3;   // 21.2-4
        hwbyte_t CR1_SPSEL          :3;   // 21.5-7 // PreScaler SELection
        hwbyte_t                    :1;   // 22.0
        hwbyte_t CR2_SCAN           :1;   // 22.1   // SCAN mode
        hwbyte_t                    :1;   // 22.2
        hwbyte_t CR2_ALIGN          :1;   // 22.3   // data ALIGNment
        hwbyte_t CR2_EXTSEL         :2;   // 22.4-5 // EXTernal event SELection
        hwbyte_t CR2_EXTTRIG        :1;   // 22.6   // EXTernal TRIGger enable
        hwbyte_t                    :1;   // 22.7
        hwbyte_t                    :6;   // 23.0-5
        hwbyte_t CR3_OVR            :1;   // 23.6   // OverRuN status flag
        hwbyte_t CR3_DBUF           :1;   // 23.7   // Data BUFfer enable
        hwword_t DR;                      // 24-25  // Data Reg
        hwword_t TDR;                     // 26-27  // schmitt Trigger Disable Reg
        hwword_t HTR;                     // 28-29  // High Threshold Reg
        hwword_t LTR;                     // 2A-2A  // Low Threshold Reg
        hwword_t AWSR;                    // 2C-2D  // Analog Watchdog Status Reg
        hwword_t AWCR;                    // 2E-2F  // Analog Watchdog Control Reg
    };
    struct {
        hwbyte_t DBRH0;                   // 00     // Data Buffer Reg 0 High
        hwbyte_t DBRL0;                   // 01     // Data Buffer Reg 0 Low
        hwbyte_t DBRH1;                   // 02     // Data Buffer Reg 0 High
        hwbyte_t DBRL1;                   // 03     // Data Buffer Reg 0 Low
        hwbyte_t DBRH2;                   // 04     // Data Buffer Reg 0 High
        hwbyte_t DBRL2;                   // 05     // Data Buffer Reg 0 Low
        hwbyte_t DBRH3;                   // 06     // Data Buffer Reg 0 High
        hwbyte_t DBRL3;                   // 07     // Data Buffer Reg 0 Low
        hwbyte_t DBRH4;                   // 08     // Data Buffer Reg 0 High
        hwbyte_t DBRL4;                   // 09     // Data Buffer Reg 0 Low
        hwbyte_t DBRH5;                   // 0A     // Data Buffer Reg 0 High
        hwbyte_t DBRL5;                   // 0B     // Data Buffer Reg 0 Low
        hwbyte_t DBRH6;                   // 0C     // Data Buffer Reg 0 High
        hwbyte_t DBRL6;                   // 0D     // Data Buffer Reg 0 Low
        hwbyte_t DBRH7;                   // 0E     // Data Buffer Reg 0 High
        hwbyte_t DBRL7;                   // 0F     // Data Buffer Reg 0 Low
        hwbyte_t DBRH8;                   // 10     // Data Buffer Reg 0 High
        hwbyte_t DBRL8;                   // 11     // Data Buffer Reg 0 Low
        hwbyte_t DBRH9;                   // 12     // Data Buffer Reg 0 High
        hwbyte_t DBRL9;                   // 13     // Data Buffer Reg 0 Low
        hwbyte_t                    :8;   // 14
        hwbyte_t                    :8;   // 15
        hwbyte_t                    :8;   // 16
        hwbyte_t                    :8;   // 17
        hwbyte_t                    :8;   // 18
        hwbyte_t                    :8;   // 19
        hwbyte_t                    :8;   // 1A
        hwbyte_t                    :8;   // 1B
        hwbyte_t                    :8;   // 1C
        hwbyte_t                    :8;   // 1D
        hwbyte_t                    :8;   // 1E
        hwbyte_t                    :8;   // 1F
        hwbyte_t CSR;                     // 20     // Control/Status Reg
        hwbyte_t CR1;                     // 21     // Configuration Reg 1
        hwbyte_t CR2;                     // 22     // Configuration Reg 2
        hwbyte_t CR3;                     // 23     // Configuration Reg 3
        hwbyte_t DRH;                     // 24     // Data Reg High
        hwbyte_t DRL;                     // 25     // Data Reg Low
        hwbyte_t TDRH;                    // 26     // schmitt Trigger Disable Reg High
        hwbyte_t TDRL;                    // 27     // schmitt Trigger Disable Reg Low
        hwbyte_t HTRH;                    // 28     // High Threshold Reg High
        hwbyte_t HTRL;                    // 29     // High Threshold Reg Low
        hwbyte_t LTRH;                    // 2A     // Low Threshold Reg High
        hwbyte_t LTRL;                    // 2B     // Low Threshold Reg Low
        hwbyte_t AWSRH;                   // 2C     // Analog Watchdog Status Reg High
        hwbyte_t AWSRL;                   // 2D     // Analog Watchdog Status Reg Low
        hwbyte_t AWCRH;                   // 2E     // Analog Watchdog Control Reg High
        hwbyte_t AWCRL;                   // 2F     // Analog Watchdog Control Reg Low
    };
} ADC_t;
ADC_t ADC                           @0x53E0;        // ADC


//  CFG section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t GCR_SWD            :1;   // 00.0   // SWim Disable
        hwbyte_t GCR_AL             :1;   // 00.1   // Activation Level
        hwbyte_t                    :6;   // 00.2-7
    };
    struct {
        hwbyte_t GCR;                     // 00     // Global Configuration Reg
    };
} CFG_t;
CFG_t CFG                           @0x7F60;


//  ITC section
//-------------------------------------------------------------------------
typedef volatile union {
    struct { // Standard names
        hwbyte_t SPR1_VECT0         :2;   // 00.0-1 VECTor 0 priority
        hwbyte_t SPR1_VECT1         :2;   // 00.2-3 VECTor 1 priority
        hwbyte_t SPR1_VECT2         :2;   // 00.4-5 VECTor 2 priority
        hwbyte_t SPR1_VECT3         :2;   // 00.6-7 VECTor 3 priority
        hwbyte_t SPR2_VECT4         :2;   // 01.0-1 VECTor 4 priority
        hwbyte_t SPR2_VECT5         :2;   // 01.2-3 VECTor 5 priority
        hwbyte_t SPR2_VECT6         :2;   // 01.4-5 VECTor 6 priority
        hwbyte_t SPR2_VECT7         :2;   // 01.6-7 VECTor 7 priority
        hwbyte_t SPR3_VECT8         :2;   // 02.0-1 VECTor 8 priority
        hwbyte_t SPR3_VECT9         :2;   // 02.2-3 VECTor 9 priority
        hwbyte_t SPR3_VECT10        :2;   // 02.4-5 VECTor 10 priority
        hwbyte_t SPR3_VECT11        :2;   // 02.6-7 VECTor 11 priority
        hwbyte_t SPR4_VECT12        :2;   // 03.0-1 VECTor 12 priority
        hwbyte_t SPR4_VECT13        :2;   // 03.2-3 VECTor 13 priority
        hwbyte_t SPR4_VECT14        :2;   // 03.4-5 VECTor 14 priority
        hwbyte_t SPR4_VECT15        :2;   // 03.6-7 VECTor 15 priority
        hwbyte_t SPR5_VECT16        :2;   // 04.0-1 VECTor 16 priority
        hwbyte_t SPR5_VECT17        :2;   // 04.2-3 VECTor 17 priority
        hwbyte_t SPR5_VECT18        :2;   // 04.4-5 VECTor 18 priority
        hwbyte_t SPR5_VECT19        :2;   // 04.6-7 VECTor 19 priority
        hwbyte_t SPR6_VECT20        :2;   // 05.0-1 VECTor 20 priority
        hwbyte_t SPR6_VECT21        :2;   // 05.2-3 VECTor 21 priority
        hwbyte_t SPR6_VECT22        :2;   // 05.4-5 VECTor 22 priority
        hwbyte_t SPR6_VECT23        :2;   // 05.6-7 VECTor 23 priority
        hwbyte_t SPR7_VECT24        :2;   // 07.0-1 VECTor 24 priority
        hwbyte_t SPR7_VECT25        :2;   // 07.2-3 VECTor 25 priority
        hwbyte_t SPR7_VECT26        :2;   // 07.4-5 VECTor 26 priority
        hwbyte_t SPR7_VECT27        :2;   // 07.6-7 VECTor 27 priority
        hwbyte_t SPR8_VECT28        :2;   // 08.0-1 VECTor 28 priority
        hwbyte_t SPR8_VECT29        :2;   // 08.2-3 VECTor 29 priority
    };
    struct { // Meaningful names
        hwbyte_t VECT_TLI           :2;   // 00.0-1 VECTor 0 priority
        hwbyte_t VECT_AWU           :2;   // 00.2-3 VECTor 1 priority
        hwbyte_t VECT_CLK           :2;   // 00.4-5 VECTor 2 priority
        hwbyte_t VECT_EXTI_PORTA    :2;   // 00.6-7 VECTor 3 priority
        hwbyte_t VECT_EXTI_PORTB    :2;   // 01.0-1 VECTor 4 priority
        hwbyte_t VECT_EXTI_PORTC    :2;   // 01.2-3 VECTor 5 priority
        hwbyte_t VECT_EXTI_PORTD    :2;   // 01.4-5 VECTor 6 priority
        hwbyte_t VECT_EXTI_PORTE    :2;   // 01.6-7 VECTor 7 priority
        hwbyte_t                    :2;   // 02.0-1 VECTor 8 priority
        hwbyte_t                    :2;   // 02.2-3 VECTor 9 priority
        hwbyte_t VECT_SPI           :2;   // 02.4-5 VECTor 10 priority
        hwbyte_t VECT_TIM1_OVF      :2;   // 02.6-7 VECTor 11 priority
        hwbyte_t VECT_TIM1_CAP      :2;   // 03.0-1 VECTor 12 priority
        hwbyte_t VECT_TIM2_OVF      :2;   // 03.2-3 VECTor 13 priority
        hwbyte_t VECT_TIM2_CAP      :2;   // 03.4-5 VECTor 14 priority
        hwbyte_t                    :2;   // 03.6-7 VECTor 15 priority
        hwbyte_t                    :2;   // 04.0-1 VECTor 16 priority
        hwbyte_t VECT_UART_TX       :2;   // 04.2-3 VECTor 17 priority
        hwbyte_t VECT_UART_RX       :2;   // 04.4-5 VECTor 18 priority
        hwbyte_t VECT_I2C           :2;   // 04.6-7 VECTor 19 priority
        hwbyte_t                    :2;   // 05.0-1 VECTor 20 priority
        hwbyte_t                    :2;   // 05.2-3 VECTor 21 priority
        hwbyte_t VECT_ADC           :2;   // 05.4-5 VECTor 22 priority
        hwbyte_t VECT_TIM4_OVF      :2;   // 05.6-7 VECTor 23 priority
        hwbyte_t VECT_FLASH         :2;   // 07.0-1 VECTor 24 priority
        hwbyte_t                    :2;   // 07.2-3 VECTor 25 priority
        hwbyte_t                    :2;   // 07.4-5 VECTor 26 priority
        hwbyte_t                    :2;   // 07.6-7 VECTor 27 priority
        hwbyte_t                    :2;   // 08.0-1 VECTor 28 priority
        hwbyte_t                    :2;   // 08.2-3 VECTor 29 priority
        hwbyte_t                    :4;   // 08.4-7
    };
    struct {
        hwbyte_t SPR1;                    // 00     // Software Priority Reg 1
        hwbyte_t SPR2;                    // 01     // Software Priority Reg 2
        hwbyte_t SPR3;                    // 02     // Software Priority Reg 3
        hwbyte_t SPR4;                    // 03     // Software Priority Reg 4
        hwbyte_t SPR5;                    // 04     // Software Priority Reg 5
        hwbyte_t SPR6;                    // 05     // Software Priority Reg 6
        hwbyte_t SPR7;                    // 06     // Software Priority Reg 7
        hwbyte_t SPR8;                    // 07     // Software Priority Reg 8
    };
} ITC_t;
ITC_t ITC                           @0x7F70;

// Interrupt priority levels
#define ITRLEVEL1  1   // Low priority
#define ITRLEVEL2  0   // Medium priority
#define ITRLEVEL3  3   // High priority


// Memory areas
//-------------------------------------------------------------------------
const hwbyte_t ROM    [0x2000]      @0x8000;
const hwbyte_t EEPROM [0x0100]      @0x1000;
      hwbyte_t RAM    [0x0400]      @0x0000;


#endif

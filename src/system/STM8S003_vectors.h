#define __STM8S003_vectors_h

/**
 * @file
 * Standard vactors table for STM8S003F3 and STM8S003K3.
 ******************************************************************************/


/**
 * Standard vector table
 * Do not modify!
 ******************************************************************************/
#pragma section const {vectors}
void (* const @vector _vectab[32])() = {
    VECTOR_RESET           ,   //     RESET
    VECTOR_TRAP            ,   //     TRAP
    VECTOR_TLI             ,   //  0  TLI
    VECTOR_AWU             ,   //  1  AWU
    VECTOR_CLK             ,   //  2  CLK
    VECTOR_EXTI_PORTA      ,   //  3  EXTI PORTA
    VECTOR_EXTI_PORTB      ,   //  4  EXTI PORTB
    VECTOR_EXTI_PORTC      ,   //  5  EXTI PORTC
    VECTOR_EXTI_PORTD      ,   //  6  EXTI PORTD
    VECTOR_EXTI_PORTE      ,   //  7  EXTI PORTE
    VECTOR_RESERVED        ,   //  8  Reserved
    VECTOR_RESERVED        ,   //  9  Reserved
    VECTOR_SPI             ,   // 10  SPI
    VECTOR_TIM1_OVF        ,   // 11  TIMER 1 OVF
    VECTOR_TIM1_CAP        ,   // 12  TIMER 1 CAP
    VECTOR_TIM2_OVF        ,   // 13  TIMER 2 OVF
    VECTOR_TIM2_CAP        ,   // 14  TIMER 2 CAP
    VECTOR_RESERVED        ,   // 15  Reserved
    VECTOR_RESERVED        ,   // 16  Reserved
    VECTOR_UART_TX         ,   // 17  UART TX
    VECTOR_UART_RX         ,   // 18  UART RX
    VECTOR_I2C             ,   // 19  I2C
    VECTOR_RESERVED        ,   // 20  Reserved
    VECTOR_RESERVED        ,   // 21  Reserved
    VECTOR_ADC             ,   // 22  ADC
    VECTOR_TIM4_OVF        ,   // 23  TIMER 4 OVF
    VECTOR_FLASH           ,   // 24  FLASH
    VECTOR_RESERVED        ,   // 25  Reserved
    VECTOR_RESERVED        ,   // 26  Reserved
    VECTOR_RESERVED        ,   // 27  Reserved
    VECTOR_RESERVED        ,   // 28  Reserved
    VECTOR_RESERVED        ,   // 29  Reserved
};
#pragma section const {}

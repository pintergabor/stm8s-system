#define __STM8S105_options_h

/**
 * @file
 * Standard options table for
 * STM8S105K4, STM8S105K6,
 * STM8S105S4, STM8S105S6,
 * STM8S105C4, STM8S105C6
 ******************************************************************************/


/**
 * Standard options table
 * In debug mode options must be set in STVD, because STVD is stupid.
 ******************************************************************************/
#ifndef __DEBUG__
#pragma section const {opt0}
const unsigned char ___opt0[] = { (unsigned char)OPT0, };
#pragma section const {opt1}
const unsigned char ___opt1[] = { (unsigned char)OPT1, (unsigned char)~OPT1, };
#pragma section const {opt2}
const unsigned char ___opt2[] = { (unsigned char)OPT2, (unsigned char)~OPT2, };
#pragma section const {opt3}
const unsigned char ___opt3[] = { (unsigned char)OPT3, (unsigned char)~OPT3, };
#pragma section const {opt4}
const unsigned char ___opt4[] = { (unsigned char)OPT4, (unsigned char)~OPT4, };
#pragma section const {opt5}
const unsigned char ___opt5[] = { (unsigned char)OPT5, (unsigned char)~OPT5, };
#pragma section const {optbl}
const unsigned char ___optbl  =   (unsigned char)OPTBL;
#pragma section const {}
#endif

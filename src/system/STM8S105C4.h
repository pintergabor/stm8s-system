#ifndef __STM8S105C4_H
#define __STM8S105C4_H

/**
 * @file
 * Header for STM8S105C4.
 ******************************************************************************/


#include "STM8S105.h"


// Memory areas
//-------------------------------------------------------------------------
const hwbyte_t ROM    [0x4000]      @0x8000;
const hwbyte_t EEPROM [0x0400]      @0x4000;
      hwbyte_t RAM    [0x0800]      @0x0000;


#endif

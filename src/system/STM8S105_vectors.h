#define __STM8S105_vectors_h

/**
 * @file
 * Standard vactors table for
 * STM8S105K4, STM8S105K6,
 * STM8S105S4, STM8S105S6,
 * STM8S105C4, STM8S105C6
 ******************************************************************************/


/**
 * Standard vector table
 * Do not modify!
 ******************************************************************************/
#pragma section const {vectors}
void (* const @vector _vectab[32])() = {
    VECTOR_RESET        ,   //    RESET
    VECTOR_TRAP         ,   //    TRAP
    VECTOR_TLI          ,   //  0 TLI
    VECTOR_AWU          ,   //  1 AWU
    VECTOR_CLK          ,   //  2 CLK
    VECTOR_EXTI_PORTA   ,   //  3 EXTI PORTA
    VECTOR_EXTI_PORTB   ,   //  4 EXTI PORTB
    VECTOR_EXTI_PORTC   ,   //  5 EXTI PORTC
    VECTOR_EXTI_PORTD   ,   //  6 EXTI PORTD
    VECTOR_EXTI_PORTE   ,   //  7 EXTI PORTE
    VECTOR_RESERVED     ,   //  8 Reserved
    VECTOR_RESERVED     ,   //  9 Reserved
    VECTOR_SPI          ,   // 10 SPI
    VECTOR_TIM1_OVF     ,   // 11 TIMER 1 OVF
    VECTOR_TIM1_CAP     ,   // 12 TIMER 1 CAP
    VECTOR_TIM2_OVF     ,   // 13 TIMER 2 OVF
    VECTOR_TIM2_CAP     ,   // 14 TIMER 2 CAP
    VECTOR_TIM3_OVF     ,   // 15 TIMER 3 OVF
    VECTOR_TIM3_CAP     ,   // 16 TIMER 3 CAP
    VECTOR_RESERVED     ,   // 17 Reserved
    VECTOR_RESERVED     ,   // 18 Reserved
    VECTOR_I2C          ,   // 19 I2C
    VECTOR_UART_TX      ,   // 20 UART TX
    VECTOR_UART_RX      ,   // 21 UART RX
    VECTOR_ADC          ,   // 22 ADC
    VECTOR_TIM4_OVF     ,   // 23 TIMER 4 OVF
    VECTOR_FLASH        ,   // 24 FLASH
    VECTOR_RESERVED     ,   // 25 Reserved
    VECTOR_RESERVED     ,   // 26 Reserved
    VECTOR_RESERVED     ,   // 27 Reserved
    VECTOR_RESERVED     ,   // 28 Reserved
    VECTOR_RESERVED     ,   // 29 Reserved
};
#pragma section const {}
